package SpockMock;

use Switch;
use Mojo::Base 'Mojo';
use Mojo::UserAgent;
use File::Slurp;
use Term::ReadLine;
use MIME::Base64;
use Encode qw(encode);
use DateTime qw();
use XML::XPath;
use XML::XPath::XMLParser;
use Data::Dumper;
use LWP;
use HTTP::Request::Common;

my $term = Term::ReadLine->new('SpockMock');

sub handler {

	my ($self, $tx) = (@_);
	my $root = '/spocmock';

	# Request
	my $method = $tx->req->method;
	my $path   = $tx->req->url->path;
	my $query  = $tx->req->url->query;
	my $ip     = $tx->remote_address;

	my $q = $query ne '' ? "?$query" : '';
	print "$ip $method $path$q\n";

	switch($method) {
		case('GET') {
			my $fi = 'index.html';
			my $ct = 'text/html';
			if($path =~ m/^\/crls\/([a-z0-9]+\.crl)$/) {
				$fi = 'crls/' . $1;
				$ct = 'application/binary';
			}
			send_response($tx, $ct, '', $fi);
		}
		case('POST') {
			if($path eq $root) {

				print '>>>' . $tx->req->body . "<<<\n";

				if($query ne '') {
					handle_outgoing_soap($tx, $query);
					print "Done\n";
				} else {
					handle_incoming_soap($tx);
					print "Done\n";
				}
			} else {
				$tx->res->code(404);
				$tx->res->headers->content_type('text/plain');
				$tx->res->body("Invalid root. Use $root");
			}
		}
	}

	# Resume transaction
	$tx->resume;
}

sub ask {
	my ($prompt) = (@_);
	return $term->readline($prompt);
}

sub send_response {

	my ($tx, $content_type, $case, $file) = (@_);

	$tx->res->code(200);
	$tx->res->headers->content_type($content_type);
	my $f = $case ? "files/cases/$case/$file" : "files/$file";
	unless(-e $f) {
		$tx->res->code(404);
		$tx->res->headers->content_type('text/html');
		$tx->res->body('<html><body>404 File not found</body></html>');
		return;
	}

	$tx->res->code(200);
	$tx->res->headers->content_type($content_type);
	my $body = read_file($f);

	my @keys = ();
	my $tmp = $body;
	while($tmp =~ m/\[\[(\w+|file:\w+)\]\]/) {
		my $key = $1;
		push @keys, $key;
		$tmp =~ s/\[\[$key\]\]//smg;
	}
	foreach my $key (sort @keys) {
		my $val = '';
		if($key =~ m/^file:/) {
			my $key2 = $key;
			$key2 =~ s/^file://;
			my $val = '';
			while(1) {
				my $file2 = ask("File for $key2?");
				last unless($file2);
				if(-e $file2) {
					my $content = read_file("files/cases/$case/$file2");
					$val = encode_base64($content);
					last;
				} else {
					print "File ./files/cases/$case/$file2 does not exist!\n";
				}
			}
		} else {
			$val = ask("Value for $key?");
			exit unless($val);
		}
		$body =~ s/\[\[$key\]\]/$val/smg;
	}

	$tx->res->body($body);
}

sub extract_request {

	my ($dir, $body) = (@_);

	my $xp = XML::XPath->new(xml => $body);

	my $base = ($xp->find('/soapenv:Envelope/soapenv:Body/*')->get_nodelist)[0];
	unless($base->getLocalName =~ m/^(.+)Request$/) {
		print "Illegal request: $base->getLocalName\n";
		return;
	}	

	my $request = $1;
	print 'Received: ' . uc($request) . ' (' . length($body). " bytes)\n";
	write_file($dir . '/' . lc($request) . '.xml', {binmode => ':raw'}, $body);

	my %varrefs = (
		'GeneralMessage' => [
			{
				'name' => 'Caller ID',
				'xpath' => './csn:callerID'
			},
			{
				'name' => 'Message ID',
				'xpath' => './csn:messageID'
			},
			{
				'name' => 'Subject',
				'xpath' => './csn:subject'
			},
			{
				'name' => 'Body',
				'xpath' => './csn:body'
			}
		],
		'GetCACertificates' => [
			{
				'name'   => 'Caller ID',
				'xpath'  => './csn:callerID'
			},
			{
				'name'   => 'Message ID',
				'xpath'  => './csn:messageID'
			}
		],
		'RequestCertificate' => [
			{
				'name'   => 'Caller ID',
				'xpath'  => './csn:callerID'
			},
			{
				'name'   => 'Message ID',
				'xpath'  => './csn:messageID'
			},
			{
				'name'   => 'Certificate Request',
				'xpath'  => './csn:certificateRequest',
				'file'   => 'cert',
				'suffix' => 'crt'
			}
		],
		'SendCertificates' => [
			{
				'name'   => 'Caller ID',
				'xpath'  => './csn:callerID'
			},
			{
				'name'   => 'Message ID',
				'xpath'  => './csn:messageID'
			},
			{
				'name'   => 'Certificate',
				'xpath'  => './csn:certificateSequence/csn:certificate',
				'file'   => 'cert',
				'suffix' => 'crt'
			}
		]
	);
	my $ref = $varrefs{$request};
	for my $extract (@$ref) {
		my $name = $$extract{'name'};
		my $xpath = $$extract{'xpath'};
		my $file = $$extract{'file'};
		my $suffix = $$extract{'suffix'};

		my $nodeset = $base->find($xpath);
		my $n = 0;
		foreach my $node ($nodeset->get_nodelist) {
			my $value = $node->string_value;

			if($file) {
				my $count = sprintf('%02d', $n++);
				$value = decode_base64($value);
				my $path = "$dir/${file}_$count.$suffix";
				write_file($path, {binmode => ':raw'}, $value);
				print "$name = saved to $path\n";
			} else {
				print "$name = $value\n";
			}
		}
	}

	my $nodeset = $xp->find('/methodCall/params/param/value/struct/member/value'); # find all paragraphs
	
}

sub handle_incoming_soap {

	my ($tx) = (@_);
	my $body = $tx->req->body;

	my $date = DateTime->now->strftime('%Y%m%d-%H%M%S');
	my $dir = "files/inbox/$date";
	mkdir($dir);
	extract_request($dir, $body);

	my $case = ask('Case?');
	send_response($tx, 'text/xml', $case, $case ? 'response.xml' : 'fail.xml');
}

sub handle_outgoing_soap {

	my ($tx, $query) = (@_);
	my $body = $tx->req->body;

	print "Sending request to $query...\n";

	my $ua = Mojo::UserAgent->new( keep_alive => 0 );
	$ua = $ua->ca('./se.crt');
	$ua = $ua->cert('./client.crt');
	$ua = $ua->key('./client.key');
	my $tx2 = $ua->post(''.$query, Content => $body);

	#print Dumper($tx2);

	$tx->res->code($tx2->res->code);
	$tx->res->headers->content_type($tx2->res->headers->content_type);
	$tx->res->content($tx2->res->content);
	$tx->res->body($tx2->res->body);
	$tx->res->error($tx2->res->error);
}

my $app = SpockMock->new();
return $app;
