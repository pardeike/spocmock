
package MyTypemaps::SPOC;
use strict;
use warnings;

our $typemap_1 = {
               'SendCertificatesRequest/certificateSequence/certificate' => 'SOAP::WSDL::XSD::Typelib::Builtin::base64Binary',
               'RequestCertificateResponse/result' => 'MyElements::RequestCertificateResponse::_result',
               'Fault/faultcode' => 'SOAP::WSDL::XSD::Typelib::Builtin::anyURI',
               'RequestCertificateRequest/certificateRequest' => 'SOAP::WSDL::XSD::Typelib::Builtin::base64Binary',
               'SendCertificatesRequest' => 'MyElements::SendCertificatesRequest',
               'GeneralMessageRequest/subject' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GeneralMessageRequest/messageID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'SendCertificatesRequest/messageID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GetCACertificatesResponse' => 'MyElements::GetCACertificatesResponse',
               'SendCertificatesRequest/statusInfo' => 'MyElements::SendCertificatesRequest::_statusInfo',
               'GetCACertificatesResponse/certificateSequence' => 'MyElements::certificateSequence::_certificateSequence',
               'SendCertificatesRequest/certificateSequence' => 'MyElements::certificateSequence::_certificateSequence',
               'GetCACertificatesResponse/certificateSequence/certificate' => 'SOAP::WSDL::XSD::Typelib::Builtin::base64Binary',
               'SendCertificatesRequest/callerID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GeneralMessageRequest' => 'MyElements::GeneralMessageRequest',
               'RequestCertificateRequest/callerID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GeneralMessageRequest/body' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GeneralMessageRequest/callerID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'RequestCertificateRequest/messageID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'RequestCertificateRequest' => 'MyElements::RequestCertificateRequest',
               'RequestCertificateResponse' => 'MyElements::RequestCertificateResponse',
               'SendCertificatesResponse/result' => 'MyElements::SendCertificatesResponse::_result',
               'GetCACertificatesRequest/callerID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'SendCertificatesResponse' => 'MyElements::SendCertificatesResponse',
               'GeneralMessageResponse/result' => 'MyElements::GeneralMessageResponse::_result',
               'GeneralMessageResponse' => 'MyElements::GeneralMessageResponse',
               'Fault/faultstring' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'RequestCertificateResponse/certificateSequence/certificate' => 'SOAP::WSDL::XSD::Typelib::Builtin::base64Binary',
               'Fault' => 'SOAP::WSDL::SOAP::Typelib::Fault11',
               'GetCACertificatesRequest' => 'MyElements::GetCACertificatesRequest',
               'GetCACertificatesResponse/result' => 'MyElements::GetCACertificatesResponse::_result',
               'RequestCertificateResponse/certificateSequence' => 'MyElements::certificateSequence::_certificateSequence',
               'Fault/faultactor' => 'SOAP::WSDL::XSD::Typelib::Builtin::token',
               'Fault/detail' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'GetCACertificatesRequest/messageID' => 'SOAP::WSDL::XSD::Typelib::Builtin::string'
             };
;

sub get_class {
  my $name = join '/', @{ $_[1] };
  return $typemap_1->{ $name };
}

sub get_typemap {
    return $typemap_1;
}

1;

__END__

__END__

=pod

=head1 NAME

MyTypemaps::SPOC - typemap for SPOC

=head1 DESCRIPTION

Typemap created by SOAP::WSDL for map-based SOAP message parsers.

=cut

